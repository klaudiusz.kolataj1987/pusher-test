module.exports = {
	globDirectory: 'public/',
    globPatterns: ["**/*.{json,ico,html,png,js,txt,css}"],
	swDest: 'public/sw.js',
    swSrc: "resources/js/sw.js",
};
