<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DemoCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'demo:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info("Cron is working fine!");
        $userController = app()->make('App\Http\Controllers\UserController');
        $users = app()->call([$userController, 'getUsers'], []);

        $chatsController = app()->make('App\Http\Controllers\ChatsController');
        $request = new \Illuminate\Http\Request();

        foreach ($users as $user) {
            $userId = $user['id'];
            $userName = $user['name'];

            app()->call([$chatsController, 'sendMessage'], [
                'id' => $userId,
                'message' => 'Sent notification to '.$userName
            ]);
        }
    }
}
