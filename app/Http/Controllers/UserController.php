<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Events\CurrencyChange;

class UserController extends Controller
{
    /**
     * Get all users
     * @param  \App\User
     * @return \Illuminate\Http\Response
     */
    public function getUsers()
    {
        $response = User::all();
        return $response;
    }
}
