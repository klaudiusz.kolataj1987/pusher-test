<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Message;
use Illuminate\Support\Facades\Auth;
use App\Events\MessageSent;
use App\Models\User;
class ChatsController extends Controller
{
    //Add the below functions
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('chat');
    }

    public function fetchMessages()
    {
        return Message::with('user')->get();
    }

    public function sendMessage($id, $message) //Request $request
    {
//        $user = Auth::user();
        $user = User::find($id);
        $messageText = $user->messages()->create([
//            'message' => $request->input('message')
        'message' => $message,
        ]);
        broadcast(new MessageSent($user, $messageText))->toOthers();
        return ['status' => 'Message Sent!'];
    }
}

