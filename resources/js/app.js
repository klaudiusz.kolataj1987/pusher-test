/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import './bootstrap';
import axios from 'axios';
import { createApp } from 'vue';

/**
 * Next, we will create a fresh Vue application instance. You may then begin
 * registering components with the application instance so they are ready
 * to use in your application's views. An example is included for you.
 */

const app = createApp({
    data(){
        return { messages: [] }
    },
    //Upon initialisation, run fetchMessages().
    created() {
        this.fetchMessages();
        window.Echo.private('chat').listen('MessageSent', (e) => {
            console.log('MessageSent from server');
                this.messages.push({
                    message: e.message.message,
                    user: e.user
                });
            this.mobileNotification();
            Notification.requestPermission().then((result) => {
                if (result === "granted") {
                    console.log('test powiadomien');
                    this.randomNotification();
                }
            });
        });

        if ("serviceWorker" in navigator && "SyncManager" in window) {
            navigator.serviceWorker.ready
                .then(function (reg) {
                    return reg.sync.register("MessageSent");
                })
                .catch(function () {
                    console.log('jest git');
                    this.mobileNotification();
                });
        } else {
            console.log('cos nie tak');
        }

        navigator.serviceWorker.ready.then((registration) => {
            registration.periodicSync.getTags().then((tags) => {
                if (tags.includes("MessageSent")) {
                    console.log("MessageSent event tag");
                }
            });
        });

        self.addEventListener("periodicsync", (event) => {
            if (event.tag == "MessageSent") {
                alert('chyba dziala');
                event.waitUntil(mobileNotification());
            }
        });

        navigator.serviceWorker.ready.then((registration) => {
            registration.periodicSync.unregister("MessageSent");
        });

        async function testPermission () {
            const status = await navigator.permissions.query({ name : "periodic-background-sync" , });
            if (status.state === "granted" ) {
                console.log('permissions granted');
            } else { console.log('not granted'); }
        }
        testPermission();
    },
    methods: {
        randomNotification() {
            const notifTitle = 'Test powiadomien';
            const notifBody = 'wyslany';
            const options = {
                body: notifBody
            };
            new Notification(notifTitle, options);
        },
        mobileNotification() {
            Notification.requestPermission((result) => {
                if (result === "granted") {
                    navigator.serviceWorker.ready.then((registration) => {
                        registration.showNotification("Vibration Sample", {
                            body: "Buzz! Buzz!",
                            icon: "/assets/icons/logo-48x48.svg",
                            vibrate: [200, 100, 200, 100, 200, 100, 200],
                            tag: "vibration-sample",
                        });
                    });
                }
            });
        },
        fetchMessages() {
            //GET request to the messages route in our Laravel server to fetch all the messages
            axios.get('/messages').then(response => {
                //Save the response in the messages array to display on the chat view
                this.messages = response.data;
            });
        },
        //Receives the message that was emitted from the ChatForm Vue component
        async addMessage(payload) {
            //Pushes it to the messages array
            //POST request to the messages route with the message data in order for our Laravel server to broadcast it.
            console.log('payload', payload)
            await axios.post('/messages', payload).then(response => {
                console.log(response.data);
            });
            this.fetchMessages();
        }
    }
});

import ExampleComponent from './components/ExampleComponent.vue';
app.component('example-component', ExampleComponent);

import ChatMessages from './components/ChatMessages.vue';
app.component('chat-messages', ChatMessages);

import ChatForm from './components/ChatForm.vue';
app.component('chat-form', ChatForm);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// Object.entries(import.meta.glob('./**/*.vue', { eager: true })).forEach(([path, definition]) => {
//     app.component(path.split('/').pop().replace(/\.\w+$/, ''), definition.default);
// });

/**
 * Finally, we will attach the application instance to a HTML element with
 * an "id" attribute of "app". This element is included with the "auth"
 * scaffolding. Otherwise, you will need to add an element yourself.
 */

app.mount('#app');
